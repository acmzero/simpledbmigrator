package com.acmzero.sdbmigrate.schema.reader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.acmzero.sdbmigrate.PostgresBaseTest;
import com.acmzero.sdbmigrate.schema.Column;
import com.acmzero.sdbmigrate.schema.Constraint;
import com.acmzero.sdbmigrate.schema.DataType;
import com.acmzero.sdbmigrate.schema.Database;
import com.acmzero.sdbmigrate.schema.RelationAction;
import com.acmzero.sdbmigrate.schema.Table;
import com.acmzero.sdbmigrate.schema.constraint.ForeignKey;
import com.acmzero.sdbmigrate.schema.constraint.PrimaryKey;

public class PostgresReaderTest extends PostgresBaseTest {

	@BeforeClass
	public static void setupDatabase() throws IOException {
		INSTANCE.initializeDatabase();
	}

	@AfterClass
	public static void removeSetupDatabase() {
		INSTANCE.removeInitDatabase();
		for (Connection conn : INSTANCE.getAllOpenedConnections()) {
//			try {
//				assertTrue(conn.isClosed());
			// disabled by now
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
		}
	}

	@Test
	public void initialLoad() throws SQLException {
		DatabaseReader reader = getReader();
		Database db = reader.readDatabase(this);
		List<Table> tables = db.getTables();
		assertTrue(!tables.isEmpty());
		checkAddressTable(db);
		checkPersonTable(db);
	}

	private void checkPersonTable(Database db) {
		Table personTable = getCheckTable("person", db);
		List<Column> columns = personTable.getColumns();
		assertTrue(!columns.isEmpty());
		validateColumn("first_name", personTable, Optional.of(DataType.CHARACTER_VARYING));
		validateColumn("last_name", personTable, Optional.of(DataType.CHARACTER_VARYING));
		validateColumn("age", personTable, Optional.of(DataType.INTEGER));
		Column id = validateColumn("person_id", personTable, Optional.of(DataType.UUID));
		assertTrue(id.isPrimaryKey());
		Column middleName = validateColumn("middle_name", personTable, Optional.of(DataType.CHARACTER_VARYING));
		assertTrue(middleName.isAllowNull());
		Column addressId = validateColumn("address_id", personTable, Optional.of(DataType.UUID));
		assertTrue(addressId.isAllowNull());
		// TODO add check for foreign table person -> address

		Optional<PrimaryKey> pkey = personTable.getPrimaryKey();
		assertTrue(pkey.isPresent());
		assertTrue(pkey.get().existsInDatabase());
		assertEquals("person_key", pkey.get().getName());
		assertEquals(id, pkey.get().getColumn());

		Optional<Constraint> addressFk = personTable.getConstraintByName("person_address");
		assertTrue(addressFk.isPresent());
		ForeignKey afk = (ForeignKey) addressFk.get();
		assertEquals("address_id", afk.getForeignColumn().getName());
		assertEquals("address", afk.getForeignColumn().getTable().getName());
		assertEquals(RelationAction.NULLIFY, afk.getOnUpdateAction());
		assertEquals(RelationAction.NO_ACTION, afk.getOnDeleteAction());
	}

	private void checkAddressTable(Database db) {
		Table addressTable = getCheckTable("address", db);
		List<Column> columns = addressTable.getColumns();
		assertTrue(!columns.isEmpty());
		Column id = validateColumn("address_id", addressTable, Optional.of(DataType.UUID));
		assertTrue(id.isPrimaryKey());
		assertTrue(!validateColumn("line1", addressTable, Optional.of(DataType.CHARACTER_VARYING)).isAllowNull());
		assertTrue(validateColumn("line2", addressTable, Optional.of(DataType.CHARACTER_VARYING)).isAllowNull());
		assertTrue(!validateColumn("zip_code", addressTable, Optional.of(DataType.CHARACTER_VARYING)).isAllowNull());

		Optional<PrimaryKey> pkey = addressTable.getPrimaryKey();
		assertTrue(pkey.isPresent());
		assertTrue(pkey.get().existsInDatabase());
		assertEquals("address_key", pkey.get().getName());
		assertEquals(id, pkey.get().getColumn());

	}

	private Table getCheckTable(String tName, Database db) {
		Optional<Table> addressTable = db.getTableByName(tName);
		assertTrue(addressTable.isPresent());
		assertTrue(addressTable.get().existsInDatabase());
		return addressTable.get();
	}

}
