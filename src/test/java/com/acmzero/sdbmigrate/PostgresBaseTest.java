package com.acmzero.sdbmigrate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Optional;
import java.util.Properties;

import com.acmzero.sdbmigrate.schema.Column;
import com.acmzero.sdbmigrate.schema.DataType;
import com.acmzero.sdbmigrate.schema.Table;
import com.acmzero.sdbmigrate.schema.reader.DatabaseReader;
import com.acmzero.sdbmigrate.schema.reader.PostgresReader;

public class PostgresBaseTest implements Context {
	protected static PostgresBaseTest INSTANCE = new PostgresBaseTest();
	private Collection<Connection> allConnections = new LinkedList<Connection>();

	public String getDatabaseName() {
		return "postgresmigratetest";
	}

	public Collection<Connection> getAllOpenedConnections() {
		return Collections.unmodifiableCollection(allConnections);
	}

	public Connection getDatabaseConnection() {
//		final String jdbcDriver = "org.postgresql.Driver"; // seems like is no longer needed
		final Properties props = getProperties();
		final String dbName = props.getProperty("db.name");
		final String user = props.getProperty("db.username");
		final String password = props.getProperty("db.password");
		final String url = props.getProperty("db.url");

		try {
			Connection conn = DriverManager.getConnection(url + "/" + dbName, user, password);
			allConnections.add(conn);
			return conn;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Error("Can't connect to database " + e.getMessage());
		}
	}

	public Properties getProperties() {
		Properties props = new Properties();
		InputStream propStream = getClass().getClassLoader().getResourceAsStream("postgres/testing.properties");
		try {
			props.load(propStream);
		} catch (IOException e) {
			e.printStackTrace();
			throw new Error("Can't read testing properties file");
		}
		return props;
	}

	public void removeInitDatabase() {
		Connection connection = getDatabaseConnection();
		executeQuery("drop table person;", connection, true);
		executeQuery("drop table address;", connection, true);
	}

	public void initializeDatabase() throws IOException {
		ContextImpl.setThreadContext(this);
		String sqlFileName = "postgres/initial-db.sql";
		InputStream sqlFile = getClass().getClassLoader().getResourceAsStream(sqlFileName);
		Connection connection = getDatabaseConnection();

		InputStreamReader isr = new InputStreamReader(sqlFile);
		BufferedReader br = new BufferedReader(isr);
		String line;
		StringBuilder sb = new StringBuilder();
		int lineNo = 0;
		while ((line = br.readLine()) != null) {
			lineNo++;
			line = line.trim();
			if ("$EOC".equals(line)) {
				// execute buffer
				executeQuery(sb.toString(), connection, false);
				sb = new StringBuilder();
			} else {
				sb.append(line).append("\n");
			}
		}
	}

	private void executeQuery(String sql, Connection connection, boolean catchException) {
		PreparedStatement ps;
		try {
			ps = connection.prepareStatement(sql);
			ps.executeUpdate();
//		connection.commit(); auto commit enabled
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
			if (!catchException) {
				throw new Error(e);
			}
		}
	}

	@Override
	public DatabaseReader getReader() {
		return new PostgresReader();
	}

	public Column validateColumn(String columnName, Table table, Optional<DataType> type) {
		Optional<Column> column = table.getColumnByName(columnName);
		assertTrue(column.isPresent());
		assertTrue(column.get().existsInDatabase());
		if (type.isPresent()) {
			assertEquals(type.get(), column.get().getDataType());
		}

		return column.get();
	}

}
