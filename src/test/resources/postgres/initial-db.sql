create table address (
	address_id uuid not null,
	line1 character varying(255) not null,
	line2 character varying(255),
	zip_code character varying(8) not null,
	constraint address_key primary key(address_id)
);
$EOC
create table person (
	person_id uuid not null,
	first_name character varying (60) not null,
	last_name character varying (60) not null,
	middle_name character varying (60),
	age int not null,
	address_id uuid,
	constraint person_key primary key (person_id),
	constraint person_address foreign key (address_id)
		references address (address_id)
		on update set null
		on delete no action
);
$EOC