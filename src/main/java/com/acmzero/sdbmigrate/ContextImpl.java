package com.acmzero.sdbmigrate;

import java.sql.Connection;

import com.acmzero.sdbmigrate.schema.reader.DatabaseReader;
import com.acmzero.sdbmigrate.schema.reader.PostgresReader;

public class ContextImpl implements Context {
	private String databaseName;
	private Connection databaseConnection;
	private static ThreadLocal<Context> localContext = new ThreadLocal<Context>();

	public String getDatabaseName() {
		return databaseName;
	}

	public Connection getDatabaseConnection() {
		return databaseConnection;
	}

	public static Context getContext() {
		if (localContext.get() != null) {
			return localContext.get();
		}
		return new ContextImpl();
	}

	/**
	 * 
	 * @return the current reader for the running instance.
	 */
	public DatabaseReader getReader() {
		return new PostgresReader();
	}

	public static void setThreadContext(Context ctx) {
		localContext.set(ctx);
	}
}
