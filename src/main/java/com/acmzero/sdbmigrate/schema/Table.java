package com.acmzero.sdbmigrate.schema;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import com.acmzero.sdbmigrate.schema.constraint.PrimaryKey;

public class Table extends DBObject {
	private List<Column> columns = new LinkedList<Column>(); // preferring ordered list to sorted
	protected Column primaryColumn;
	private Collection<Constraint> constraints = new LinkedList<Constraint>();

	@Override
	public ObjectType getType() {
		return ObjectType.TABLE;
	}

	public List<Column> getColumns() {
		return Collections.unmodifiableList(columns);
	}

	/**
	 * Returns first column with the provided name, case insensitive
	 */
	public Optional<Column> getColumnByName(String columnName) {
		return findFirst(columns, columnName);
	}

	public Optional<Constraint> getConstraintByName(String constName) {
		return findFirst(constraints, constName);
	}

	protected void addColumn(Column c) {
		if (c.isPrimaryKey()) {
			if (this.primaryColumn != null) {
				throw new IllegalStateException();
			}
			this.primaryColumn = c;
		}
		this.columns.add(Objects.requireNonNull(c));
	}

	protected void addConstraint(Constraint c) {
		this.constraints.add(c);
	}

	public Optional<PrimaryKey> getPrimaryKey() {
		Optional<Constraint> result = this.constraints.stream().filter(c -> {
			return c.getType() == ObjectType.PRIMARY_KEY;
		}).findFirst();
		if (result.isPresent()) {
			return Optional.of((PrimaryKey) result.get());
		}
		return Optional.empty();
	}

}
