package com.acmzero.sdbmigrate.schema;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

import com.acmzero.sdbmigrate.Context;
import com.acmzero.sdbmigrate.ContextImpl;
import com.acmzero.sdbmigrate.schema.reader.DatabaseReader;

public abstract class DBObject implements Nameable {
	private String name;

	public final boolean existsInDatabase() {
		Context context = ContextImpl.getContext();
		DatabaseReader reader = context.getReader();
		return reader.databaseObjectExists(this, context);
	}

	public String getName() {
		return name;
	}

	public void setName(String value) {
		this.name = Objects.requireNonNull(value, "Database object should have a valid name");
	}

	public abstract ObjectType getType();

	protected <T extends DBObject> Optional<T> findFirst(Collection<T> collection, String _objectName) {
		String lowerName = _objectName.toLowerCase();
		return collection.stream().filter((DBObject o) -> o.getName().toLowerCase().equals(lowerName)).findFirst();
	}

}
