package com.acmzero.sdbmigrate.schema;

import java.util.List;

public class Index extends DBObject {
	private List<IndexColumn> columns;

	@Override
	public ObjectType getType() {
		return ObjectType.INDEX;
	}

}
