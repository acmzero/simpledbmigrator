package com.acmzero.sdbmigrate.schema;

/**
 * Represents a constraints, in PG a constraint is:
 * 
 * A Foreign Key
 * 
 * A Primary Key
 * 
 * A Check
 * 
 * @author acmzero
 *
 */
public class Constraint extends DBObject {
	private Table parentTable;

	@Override
	public ObjectType getType() {
		return ObjectType.CONSTRAINT;
	}

	public void setTable(Table t) {
		this.parentTable = t;
	}

	public Table getTable() {
		return parentTable;
	}
}
