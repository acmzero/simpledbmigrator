package com.acmzero.sdbmigrate.schema;

public interface Nameable {

	public String getName();

}
