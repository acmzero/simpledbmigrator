package com.acmzero.sdbmigrate.schema;

public enum RelationAction {
	CASCADE, NO_ACTION, NULLIFY, RESTRICT;

}
