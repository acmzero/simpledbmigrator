package com.acmzero.sdbmigrate.schema;

public enum ObjectType {
	DATABASE, COLUMN, TABLE, CONSTRAINT, FOREIGN_KEY, PRIMARY_KEY, CHECK, INDEX, INDEX_COLUMN, VIEW, SEQUENCE, FUNCTION;
}
