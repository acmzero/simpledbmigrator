package com.acmzero.sdbmigrate.schema;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class Database extends DBObject {
	private final List<Table> tables = new LinkedList<Table>();;

	@Override
	public final ObjectType getType() {
		return ObjectType.DATABASE;
	}

	public List<Table> getTables() {
		return Collections.unmodifiableList(tables);
	}

	/**
	 * Returns the first match of the provided table name, case insensitive
	 */
	public Optional<Table> getTableByName(String _tableName) {
		return findFirst(tables, _tableName);
	}

	public void addTable(Table table) {
		this.tables.add(table);
	}

}
