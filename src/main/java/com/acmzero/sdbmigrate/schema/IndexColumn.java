package com.acmzero.sdbmigrate.schema;

public class IndexColumn extends Column {

	/**
	 * Using Boxing instead of primitive boolean to say where asc/desc is undefined
	 * and/or for special indexes
	 */
	private Boolean isAscendent;
	private String specialColumnDefinition;
	private boolean isUnique;

	@Override
	public ObjectType getType() {
		return ObjectType.INDEX_COLUMN;
	}

}
