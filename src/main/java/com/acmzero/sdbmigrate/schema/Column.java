package com.acmzero.sdbmigrate.schema;

public class Column extends DBObject {
	protected DataType columnType;
	protected int size;
	protected boolean isAllowNull;
	protected String defaultValueExpresion;
	protected boolean isPrimaryKey = false;
	protected boolean isAutoIncrement;
	protected Table parentTable;

	@Override
	public ObjectType getType() {
		return ObjectType.COLUMN;
	}

	public DataType getDataType() {
		return columnType;
	}

	public boolean isPrimaryKey() {
		return isPrimaryKey;
	}

	public boolean isAllowNull() {
		return isAllowNull;
	}

	public Table getTable() {
		return parentTable;
	}

	public void setTable(Table v) {
		this.parentTable = v;
	}

	public void setPrimaryKey(boolean v) {
		this.isPrimaryKey = v;
	}

}
