package com.acmzero.sdbmigrate.schema.constraint;

import com.acmzero.sdbmigrate.schema.Column;
import com.acmzero.sdbmigrate.schema.Constraint;
import com.acmzero.sdbmigrate.schema.RelationAction;

public class ForeignKey extends Constraint {
	private Column foreignColumn;
	private RelationAction onUpdateAction;
	private RelationAction onDeleteAction;

	public Column getForeignColumn() {
		return foreignColumn;
	}

	public void setForeignColumn(Column foreignColumn) {
		this.foreignColumn = foreignColumn;
	}

	public RelationAction getOnUpdateAction() {
		return onUpdateAction;
	}

	public void setOnUpdateAction(RelationAction onUpdateAction) {
		this.onUpdateAction = onUpdateAction;
	}

	public RelationAction getOnDeleteAction() {
		return onDeleteAction;
	}

	public void setOnDeleteAction(RelationAction onDeleteAction) {
		this.onDeleteAction = onDeleteAction;
	}

}
