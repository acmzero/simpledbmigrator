package com.acmzero.sdbmigrate.schema.constraint;

import com.acmzero.sdbmigrate.schema.Column;
import com.acmzero.sdbmigrate.schema.Constraint;
import com.acmzero.sdbmigrate.schema.ObjectType;

public class PrimaryKey extends Constraint {
	private Column column;

	@Override
	public ObjectType getType() {
		return ObjectType.PRIMARY_KEY;
	}

	public Column getColumn() {
		return column;
	}

	public void setColumn(Column column) {
		this.column = column;
		setTable(column.getTable());
	}

}
