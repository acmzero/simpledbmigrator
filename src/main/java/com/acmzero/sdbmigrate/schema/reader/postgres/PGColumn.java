package com.acmzero.sdbmigrate.schema.reader.postgres;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.acmzero.sdbmigrate.Context;
import com.acmzero.sdbmigrate.schema.Column;
import com.acmzero.sdbmigrate.schema.DataType;
import com.acmzero.sdbmigrate.schema.Table;

public class PGColumn extends Column {

	public PGColumn(String columnName, Table table) {
		setName(columnName);
		setTable(table);
	}

	public void loadDefinition(Context context, Table table) {
		String sql = "Select * from information_schema.columns where column_name = ? and table_name = ? ";
		PreparedStatement ps;
		try {
			ps = context.getDatabaseConnection().prepareStatement(sql);
			ps.setString(1, getName());
			ps.setString(2, table.getName());
			ps.execute();
			ResultSet rs = ps.getResultSet();
			while (rs.next()) {
				String dataTypeStr = rs.getString("data_type");
				this.isAllowNull = rs.getBoolean("is_nullable");
				setDataTypeFromName(rs.getString("data_type"));

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void setDataTypeFromName(String dataTypeName) {
		switch (dataTypeName.toLowerCase()) {
		case "character varying":
			columnType = DataType.CHARACTER_VARYING;
			break;
		case "uuid":
			columnType = DataType.UUID;
			break;
		case "integer":
			columnType = DataType.INTEGER;
			break;

		default:
			columnType = DataType.UNKNOWN;
			break;
		}
	}

}
