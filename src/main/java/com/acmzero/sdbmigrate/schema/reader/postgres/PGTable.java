package com.acmzero.sdbmigrate.schema.reader.postgres;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import com.acmzero.sdbmigrate.Context;
import com.acmzero.sdbmigrate.schema.Column;
import com.acmzero.sdbmigrate.schema.Constraint;
import com.acmzero.sdbmigrate.schema.Table;
import com.acmzero.sdbmigrate.schema.constraint.PrimaryKey;
import com.acmzero.sdbmigrate.schema.reader.PostgresReader;

public class PGTable extends Table {
	private boolean isLoaded = false;

	public PGTable(String _tableName) {
		setName(_tableName);
	}

	public void loadDefinition(Context context, PostgresReader postgresReader) {
		// load columns
		if (isLoaded) {
			return;
		}
		String sql = "select column_name from information_schema.columns where table_name = ? order by ordinal_position";

		try {
			PreparedStatement ps = context.getDatabaseConnection().prepareStatement(sql);
			ps.setString(1, getName());
			ps.execute();
			ResultSet rs = ps.getResultSet();
			while (rs.next()) {
				String columnName = rs.getString(1);
				PGColumn column = new PGColumn(columnName, this);
				column.loadDefinition(context, this);
				addColumn(column);
			}
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String pkSql = "select kcu.column_name, tco.constraint_name from information_schema.table_constraints tco\n"
				+ "join information_schema.key_column_usage kcu \n"
				+ "     on kcu.constraint_name = tco.constraint_name\n"
				+ "     and kcu.constraint_schema = tco.constraint_schema\n"
				+ "     and kcu.constraint_name = tco.constraint_name\n" + "where tco.constraint_type = 'PRIMARY KEY'\n"
				+ "and kcu.table_name = ? ";
		try {
			PreparedStatement ps = context.getDatabaseConnection().prepareStatement(pkSql);
			ps.setString(1, getName());
			ps.execute();
			ResultSet rs = ps.getResultSet();
			while (rs.next()) {
				String pkColumnName = rs.getString(1);
				Optional<Column> pkColumn = getColumnByName(pkColumnName);
				if (pkColumn.isPresent()) {
					pkColumn.get().setPrimaryKey(true);
					primaryColumn = pkColumn.get();
					PrimaryKey pKey = new PrimaryKey();
					pKey.setName(rs.getString(2));
					pKey.setColumn(primaryColumn);
					addConstraint(pKey);
				}
			}
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		String fkSql = "select tco.constraint_name from information_schema.table_constraints tco where table_name = ? and constraint_type = 'FOREIGN KEY'";
		try {
			PreparedStatement ps = context.getDatabaseConnection().prepareStatement(fkSql);
			ps.setString(1, getName());
			ps.execute();
			ResultSet rs = ps.getResultSet();
			while (rs.next()) {
				String fkName = rs.getString(1);
				PGForeignKey fk = new PGForeignKey();
				postgresReader.addAfterTableHook(db -> {
					fk.loadDefinition(context, this, fkName, db);
					this.addConstraint(fk);
				});
			}
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		isLoaded = true;
	}

}
