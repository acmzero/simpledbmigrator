package com.acmzero.sdbmigrate.schema.reader.postgres;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.acmzero.sdbmigrate.Context;
import com.acmzero.sdbmigrate.schema.Column;
import com.acmzero.sdbmigrate.schema.Database;
import com.acmzero.sdbmigrate.schema.RelationAction;
import com.acmzero.sdbmigrate.schema.Table;
import com.acmzero.sdbmigrate.schema.constraint.ForeignKey;

public class PGForeignKey extends ForeignKey {

	protected void loadDefinition(Context context, Table table, String constraintName, Database db) {
		setName(constraintName);
		setTable(table);
		String sql = String.format(
				"SELECT pg_catalog.pg_get_constraintdef(r.oid, false) as condef\n" + "FROM pg_catalog.pg_constraint r\n"
						+ "WHERE r.conrelid = '%s'::regclass AND r.contype = 'f'\n" + "and conname = ?",
				table.getName());

		try {
			PreparedStatement ps = context.getDatabaseConnection().prepareStatement(sql);
			ps.setString(1, constraintName);
			ps.execute();
			ResultSet rs = ps.getResultSet();
			while (rs.next()) {
				String definition = rs.getString(1);
				String foreignTableName = parseForeignTable(definition);
				String foreignColumnName = parseForeignColumn(definition);
				Table foreignTable = db.getTableByName(foreignTableName).get();
				Column fColumn = foreignTable.getColumnByName(foreignColumnName).get();
				setForeignColumn(fColumn);

				RelationAction update = parseOn(definition, true);
				RelationAction delete = parseOn(definition, false);
				setOnDeleteAction(delete);
				setOnUpdateAction(update);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private RelationAction parseOn(String definition, boolean update) {
		String[] split = definition.split(String.format(" ON %s ", update ? "UPDATE" : "DELETE"));
		String action = "";
		if (split.length == 2) {
			String[] uSplit = split[1].split(" ON");
			if (uSplit.length == 2) {
				action = uSplit[0];
			} else {
				action = split[1];
			}
		}
		return getRelationAction(action);
	}

	private RelationAction getRelationAction(String action) {
		switch (action) {
		case "CASCADE":
			return RelationAction.CASCADE;
		case "RESTRICT":
			return RelationAction.RESTRICT;
		case "SET NULL":
			return RelationAction.NULLIFY;
		default:
			return RelationAction.NO_ACTION;
		}
	}

	private String parseForeignColumn(String definition) {
		Pattern pattern = Pattern.compile("REFERENCES .*\\((.*?)\\)");
		Matcher matcher = pattern.matcher(definition);
		if (matcher.find()) {
			return matcher.group(1);
		}
		return null;
	}

	private String parseForeignTable(String definition) {
		Pattern pattern = Pattern.compile("REFERENCES (.*?)\\(");
		Matcher matcher = pattern.matcher(definition);
		if (matcher.find()) {
			return matcher.group(1);
		}
		return null;
	}

}
