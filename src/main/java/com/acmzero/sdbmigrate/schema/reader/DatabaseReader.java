package com.acmzero.sdbmigrate.schema.reader;

import java.sql.SQLException;

import com.acmzero.sdbmigrate.Context;
import com.acmzero.sdbmigrate.schema.DBObject;
import com.acmzero.sdbmigrate.schema.Database;

public interface DatabaseReader {

	public Database readDatabase(Context context) throws SQLException;

	public boolean databaseObjectExists(DBObject dbObject, Context context);

}
