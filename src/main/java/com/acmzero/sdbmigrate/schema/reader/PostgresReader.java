package com.acmzero.sdbmigrate.schema.reader;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.function.Consumer;
import java.util.function.Function;

import com.acmzero.sdbmigrate.Context;
import com.acmzero.sdbmigrate.schema.Column;
import com.acmzero.sdbmigrate.schema.DBObject;
import com.acmzero.sdbmigrate.schema.Database;
import com.acmzero.sdbmigrate.schema.ObjectType;
import com.acmzero.sdbmigrate.schema.constraint.PrimaryKey;
import com.acmzero.sdbmigrate.schema.reader.postgres.PGTable;

public class PostgresReader implements DatabaseReader {

	protected Collection<Consumer<Database>> afterTableHooks = new LinkedList<Consumer<Database>>();

	public Database readDatabase(Context context) throws SQLException {
		Database db = new Database();
		Connection conn = context.getDatabaseConnection();
		String allTablesSql = " select relname from pg_class where relkind = 't';";
		allTablesSql = "SELECT table_name FROM information_schema.tables WHERE table_schema='public'";
		PreparedStatement ps = conn.prepareStatement(allTablesSql);
		ps.execute();

		ResultSet rs = ps.getResultSet();
		while (rs.next()) {
			String tableName = rs.getString(1);
			PGTable table = new PGTable(tableName);
			table.loadDefinition(context, this);
			db.addTable(table);
		}
		ps.close();
		for (Consumer<Database> fn : afterTableHooks) {
			fn.accept(db);
		}

		return db;
	}

	public void addAfterTableHook(Consumer<Database> fn) {
		afterTableHooks.add(fn);
	}

	@Override
	public boolean databaseObjectExists(DBObject dbObject, Context ctx) {
		// by type check if exists in database
		String tableName = "";
		String where = "";
		switch (dbObject.getType()) {
		case TABLE:
			tableName = "tables";
			where = String.format(" table_name = '%s' ", dbObject.getName());
			break;
		case COLUMN:
			tableName = "columns";
			where = String.format("column_name = '%s' and table_name = '%s' ", dbObject.getName(),
					((Column) dbObject).getTable().getName());
			break;
		case PRIMARY_KEY:
			PrimaryKey pkey = (PrimaryKey) dbObject;
			tableName = "table_constraints";
			where = String.format(" table_name = '%s' and constraint_name = '%s' and constraint_type = 'PRIMARY KEY' ",
					pkey.getTable().getName(), pkey.getName());
			break;

		default:
			break;

		}
		StringBuilder sb = new StringBuilder();
		sb.append("Select 1 from information_schema.");
		sb.append(tableName).append(" where ");
		sb.append(where);

		PreparedStatement ps;
		try {
			ps = ctx.getDatabaseConnection().prepareStatement(sb.toString());
			ps.execute();
			ResultSet rs = ps.getResultSet();
			return rs.next();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

	public static interface SimpleFunction {
		public void apply(Database db);
	}
}
