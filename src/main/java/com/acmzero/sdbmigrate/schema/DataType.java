package com.acmzero.sdbmigrate.schema;

public enum DataType {
	UNKNOWN, CHARACTER_VARYING, UUID, INTEGER;

}
