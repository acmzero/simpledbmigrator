package com.acmzero.sdbmigrate;

import java.sql.Connection;

import com.acmzero.sdbmigrate.schema.reader.DatabaseReader;

public interface Context {

	public String getDatabaseName();

	public Connection getDatabaseConnection();

	public DatabaseReader getReader();

}
